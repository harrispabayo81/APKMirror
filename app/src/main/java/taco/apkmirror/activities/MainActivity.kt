package taco.apkmirror.activities

import android.Manifest
import android.animation.*
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.content.res.ColorStateList
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.LightingColorFilter
import android.nfc.NdefMessage
import android.nfc.NdefRecord
import android.nfc.NfcAdapter
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import android.view.animation.DecelerateInterpolator
import android.webkit.WebChromeClient
import android.webkit.WebView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.edit
import androidx.core.graphics.toColorInt
import androidx.core.view.isGone
import androidx.core.view.isVisible
import androidx.preference.PreferenceManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomnavigation.BottomNavigationView.OnNavigationItemReselectedListener
import im.delight.android.webview.AdvancedWebView
import taco.apkmirror.BuildConfig
import taco.apkmirror.R
import taco.apkmirror.classes.PageAsync
import taco.apkmirror.databinding.ActivityMainBinding
import taco.apkmirror.databinding.DialogEdittextBinding
import taco.apkmirror.interfaces.AsyncResponse
import taco.apkmirror.util.copyToClipboard
import taco.apkmirror.util.displayToast
import taco.apkmirror.util.startIntent

class MainActivity : AppCompatActivity(), AdvancedWebView.Listener, AsyncResponse {
    private val navigationHome = R.id.navigation_home
    private val navigationUpload = R.id.navigation_upload
    private val navigationSettings = R.id.navigation_settings
    private val navigationExit = R.id.navigation_exit
    private var saveUrl = false
    private var shortAnimDuration: Int? = null
    private var settingsShortcut = false
    private var triggerAction = true
    private var previsionThemeColor = "#FF8B14".toColorInt()
    private var sharedPreferences: SharedPreferences? = null
    private var nfcAdapter: NfcAdapter? = null
    private var _binding: ActivityMainBinding? = null
    private val binding get() = _binding!!

    /**
     * Listens for user clicking on the tab again.
     * We first check if the page is scrolled.
     * If so we move to top, otherwise we refresh the page.
     */
    private val tabReselectListener = OnNavigationItemReselectedListener { menuItem: MenuItem ->
        val itemId = menuItem.itemId
        loadUrlToWebview(
            when (itemId) {
                navigationHome -> APKMIRROR_URL
                navigationUpload -> APKMIRROR_UPLOAD_URL
                else -> null
            }
        )
    }
    private val chromeClient: WebChromeClient = object : WebChromeClient() {
        override fun onProgressChanged(v: WebView, progress: Int) {
            // update the progressbar value
            val oa = ObjectAnimator.ofInt(binding.mainProgressBar, "progress", progress)
            oa.duration = 100 // 0.5 second
            oa.interpolator = DecelerateInterpolator()
            oa.start()
        }
    }
    private val tabSelectListener =
        BottomNavigationView.OnNavigationItemSelectedListener { menuItem ->
            if (triggerAction) {
                when (menuItem.itemId) {
                    navigationHome ->
                        //Home pressed
                        if (binding.settingsLayoutFragment.isVisible) {
                            //settings is visible, gonna hide it
                            if (binding.mainWebview.url == APKMIRROR_UPLOAD_URL) {
                                binding.mainWebview.loadUrl(APKMIRROR_URL)
                            }
                            crossFade(binding.settingsLayoutFragment, binding.webContainer)
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                changeUIColor(previsionThemeColor)
                            }
                        } else {
                            //settings is not visible, load url
                            binding.mainWebview.loadUrl(APKMIRROR_URL)
                        }
                    navigationUpload ->
                        //Upload pressed
                        if (binding.settingsLayoutFragment.isVisible) {
                            //settings is visible, gonna hide it
                            if (binding.mainWebview.url != APKMIRROR_UPLOAD_URL) {
                                binding.mainWebview.loadUrl(APKMIRROR_UPLOAD_URL)
                            }
                            crossFade(binding.settingsLayoutFragment, binding.webContainer)
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                changeUIColor(previsionThemeColor)
                            }
                        } else {
                            //settings is not visible, load url
                            binding.mainWebview.loadUrl(APKMIRROR_UPLOAD_URL)
                        }
                    navigationSettings -> {
                        //Settings pressed
                        if (binding.firstLoadingView.isVisible) {
                            binding.firstLoadingView.isGone = true
                        }
                        crossFade(binding.webContainer, binding.settingsLayoutFragment)
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            changeUIColor(
                                ContextCompat.getColor(
                                    this@MainActivity,
                                    R.color.colorPrimary
                                )
                            )
                        }
                    }
                    navigationExit ->
                        //Exit pressed
                        finish()
                }
            }
            triggerAction = true
            true
        }

    private fun loadUrlToWebview(url: String?) {
        if (url == null) {
            return
        }
        val webScrollY = binding.mainWebview.scrollY
        //Upload re-pressed
        if (webScrollY == 0) {
            //Load url
            binding.mainWebview.loadUrl(url)
        } else {
            //Scroll to top
            binding.mainWebview.scrollY = 0
        }
    }

    private fun initNavigation() {
        //Making the bottom navigation do something
        binding.navigation.setOnNavigationItemSelectedListener(tabSelectListener)
        binding.navigation.setOnNavigationItemReselectedListener(tabReselectListener)
        if (sharedPreferences!!.getBoolean("show_exit", false)) {
            binding.navigation.inflateMenu(R.menu.navigation_exit)
            binding.navigation.invalidate()
        }
    }

    private fun initSearchFab() {
        val fab = sharedPreferences!!.getBoolean("fab", true)
        if (fab) {
            binding.fabSearch.show()
            binding.fabSearch.setOnClickListener { search() }
        }
    }

    private fun initWebView(url: String) {
        binding.mainWebview.setListener(this, this)
        binding.mainWebview.addPermittedHostname("apkmirror.com")
        binding.mainWebview.webChromeClient = chromeClient
        binding.mainWebview.setUploadableFileTypes("application/vnd.android.package-archive")
        binding.mainWebview.loadUrl(url)
        binding.refreshLayout.setOnRefreshListener { binding.mainWebview.reload() }
    }

    override fun onResume() {
        super.onResume()
        binding.mainWebview.onResume()
    }

    override fun onPause() {
        binding.mainWebview.onPause()
        super.onPause()
    }

    override fun onDestroy() {
        binding.mainWebview.onDestroy()
        super.onDestroy()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            setTheme(R.style.AppTheme)
            _binding = ActivityMainBinding.inflate(layoutInflater)
            setContentView(binding.root)
            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
            initSearchFab()
            nfcAdapter = NfcAdapter.getDefaultAdapter(this)
            shortAnimDuration = resources.getInteger(android.R.integer.config_shortAnimTime)
            initNavigation()
            saveUrl = sharedPreferences!!.getBoolean("save_url", false)
            val url: String
            val link = intent
            val data = link.data
            if (data != null) {
                //app was opened from browser
                url = "$data"
            } else {
                //data is null which means it was either launched from shortcuts or normally
                val bundle = link.extras
                if (bundle == null) {
                    //Normal start from launcher
                    url = if (saveUrl) {
                        sharedPreferences!!.getString("last_url", APKMIRROR_URL)!!
                    } else {
                        APKMIRROR_URL
                    }
                } else {
                    //Ok it was shortcuts, check if it was settings
                    val bundleUrl = bundle.getString("url")
                    if (bundleUrl != null) {
                        if (bundleUrl == "apkmirror://settings") {
                            //It was settings
                            url = APKMIRROR_URL
                            binding.navigation.selectedItemId = navigationSettings
                            crossFade(binding.webContainer, binding.settingsLayoutFragment)
                            settingsShortcut = true
                        } else {
                            url = bundleUrl
                        }
                    } else {
                        url = if (saveUrl) {
                            sharedPreferences!!.getString("last_url", APKMIRROR_URL)!!
                        } else {
                            APKMIRROR_URL
                        }
                    }
                }
            }
            initWebView(url)

            // I know not the best solution xD
            if (!settingsShortcut) {
                binding.firstLoadingView.isVisible = true
                Handler().postDelayed({
                    if (binding.firstLoadingView.isVisible) {
                        crossFade(binding.firstLoadingView, binding.webContainer)
                    }
                }, 2000)
            }
        } catch (e: RuntimeException) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
            AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(R.string.runtime_error_dialog_content)
                .setPositiveButton(android.R.string.ok) { _, _ -> finish() }
                .setNeutralButton(R.string.copy_log) { _, _ ->
                    this@MainActivity.copyToClipboard("log", "$e")
                }
                .show()
        }
    }

    override fun onStop() {
        if (saveUrl && binding.mainWebview.url != "apkmirror://settings") {
            sharedPreferences!!.edit {
                putString("last_url", binding.mainWebview.url)
            }
        }
        super.onStop()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, i: Intent?) {
        super.onActivityResult(requestCode, resultCode, i)
        binding.mainWebview.onActivityResult(requestCode, resultCode, i)
    }

    override fun onBackPressed() {
        if (binding.settingsLayoutFragment.isVisible) {
            crossFade(binding.settingsLayoutFragment, binding.webContainer)
            if (binding.mainWebview.url == APKMIRROR_UPLOAD_URL) {
                triggerAction = false
                binding.navigation.selectedItemId = navigationUpload
            } else {
                triggerAction = false
                binding.navigation.selectedItemId = navigationHome
            }
            return
        }
        if (!binding.mainWebview.onBackPressed()) {
            return
        }
        super.onBackPressed()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        // Next line causes crash
        //webView.saveState(outState);
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        // Next line causes crash
        //webView.restoreState(savedInstanceState);
    }

    private fun runAsync(url: String) {
        //getting apps
        val pageAsync = PageAsync()
        pageAsync.response = this@MainActivity
        pageAsync.execute(url)
    }

    private fun search() {
        val dialogBinding = DialogEdittextBinding.inflate(layoutInflater)

        AlertDialog.Builder(this)
            .setTitle(R.string.search)
            .setView(dialogBinding.root)
            .setNegativeButton(android.R.string.cancel, null)
            .setPositiveButton(android.R.string.ok) { _, _ ->
                binding.mainWebview.loadUrl(
                    "https://www.apkmirror.com/?s="
                            + dialogBinding.editText.text.toString()
                )
            }
            .show()
    }

    private fun crossFade(toHide: View, toShow: View) {
        toShow.alpha = 0f
        toShow.isVisible = true
        toShow.animate()
            .alpha(1f)
            .setDuration(shortAnimDuration!!.toLong())
            .setListener(null)
        toHide.animate()
            .alpha(0f)
            .setDuration(shortAnimDuration!!.toLong())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(a: Animator) {
                    toHide.isGone = true
                }
            })
    }

    private fun download(url: String, name: String) {
        if (sharedPreferences!!.getBoolean("external_download", false)) {
            startIntent(url)
        } else {
            if (AdvancedWebView.handleDownload(this, url, name)) {
                displayToast(R.string.download_started)
            } else {
                displayToast(R.string.cant_download)
            }
        }
    }

    private val isWritePermissionGranted: Boolean
        get() {
            return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                    checkSelfPermission(
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                    ) == PackageManager.PERMISSION_GRANTED
        }

    override fun onProcessFinish(themeColor: Int) {
        // updating interface
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            changeUIColor(themeColor)
        }
        previsionThemeColor = themeColor
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun changeUIColor(color: Int) {
        val anim = ValueAnimator.ofArgb(previsionThemeColor, color)
        anim.setEvaluator(ArgbEvaluator())
        anim.addUpdateListener { vA: ValueAnimator ->
            val getAV = vA.animatedValue as Int
            binding.mainProgressBar.progressDrawable.colorFilter =
                LightingColorFilter(-0x1000000, getAV)
            setSystemBarColor(getAV)
            val colorStateList =
                ColorStateList(COLOR_STATES, intArrayOf(vA.animatedValue as Int, R.color.inactive))
            binding.navigation.itemTextColor = colorStateList
            binding.navigation.itemIconTintList = colorStateList
            binding.fabSearch.backgroundTintList = ColorStateList.valueOf(getAV)
        }
        anim.duration = shortAnimDuration!!.toLong()
        anim.start()
        binding.refreshLayout.setColorSchemeColors(color, color, color)
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private fun setSystemBarColor(color: Int) {
        val clr: Int
        //this makes the color darker or use a nicer orange color
        // Note: Do NOT replace this with previsionThemeColor
        if (color == "#FF8B14".toColorInt()) {
            clr = "#F47D20".toColorInt()
        } else {
            val hsv = FloatArray(3)
            Color.colorToHSV(color, hsv)
            hsv[2] *= 0.8f
            clr = Color.HSVToColor(hsv)
        }
        val w = this@MainActivity.window
        w.statusBarColor = clr
        if (sharedPreferences!!.getBoolean("color_navigation_bar", false)) {
            w.navigationBarColor = clr
        }
    }

    private fun setupNFC(url: String) {
        if (nfcAdapter != null) { // in case there is no NFC
            try {
                // create an NDEF message containing the current URL:
                val rec = NdefRecord.createUri(url) // url: current URL (String or Uri)
                val ndef = NdefMessage(rec)
                // make it available via Android Beam:
                nfcAdapter!!.setNdefPushMessage(ndef, this, this)
            } catch (e: IllegalStateException) {
                e.printStackTrace()
            }
        }
    }

    override fun onPageFinished(url: String) {
        binding.mainProgressBarContainer.animate()
            .alpha(0f)
            .setDuration(resources.getInteger(android.R.integer.config_longAnimTime).toLong())
            .setListener(object : AnimatorListenerAdapter() {
                override fun onAnimationEnd(animation: Animator) {
                    super.onAnimationEnd(animation)
                    binding.mainProgressBarContainer.isGone = true
                }
            })
        if (binding.refreshLayout.isRefreshing) {
            binding.refreshLayout.isRefreshing = false
        }
    }

    //WebView factory methods below
    override fun onPageStarted(url: String, favicon: Bitmap?) {
        if (!url.contains("https://www.apkmirror.com/wp-content/")) {
            runAsync(url)
            setupNFC(url)
            when (binding.navigation.selectedItemId) {
                navigationHome ->
                    if (url == APKMIRROR_UPLOAD_URL) {
                        triggerAction = false
                        binding.navigation.selectedItemId = navigationUpload
                    }
                navigationUpload ->
                    if (url != APKMIRROR_UPLOAD_URL) {
                        triggerAction = false
                        binding.navigation.selectedItemId = navigationHome
                    }
            }

            //Showing progress bar
            binding.mainProgressBarContainer.animate()
                .alpha(1f)
                .setDuration(shortAnimDuration!!.toLong())
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationStart(a: Animator) {
                        super.onAnimationStart(a)
                        binding.mainProgressBarContainer.isVisible = true
                    }
                })
        }
    }

    override fun onPageError(errorCode: Int, description: String, failingUrl: String) {
        if (errorCode == -2) {
            AlertDialog.Builder(this)
                .setTitle(R.string.error)
                .setMessage(
                    getString(R.string.error_while_loading_page) + " " + failingUrl +
                            "(" + errorCode + " " + description + ")"
                )
                .setPositiveButton(R.string.refresh) { dialog, _ ->
                    binding.mainWebview.reload()
                    dialog.dismiss()
                }
                .setNegativeButton(R.string.dismiss) { dialog, _ ->
                    dialog.dismiss()
                }
                .show()
        }
    }

    override fun onDownloadRequested(
        url: String, suggestedFilename: String, mimeType: String,
        contentLength: Long, contentDisposition: String, userAgent: String
    ) {
        if (isWritePermissionGranted) {
            download(url, suggestedFilename)
        } else {
            AlertDialog.Builder(this@MainActivity)
                .setTitle(R.string.write_permission)
                .setMessage(R.string.storage_access)
                .setPositiveButton(R.string.request_permission) { _, _ ->
                    //Request permission
                    ActivityCompat.requestPermissions(
                        this@MainActivity, arrayOf(
                            Manifest.permission.WRITE_EXTERNAL_STORAGE
                        ), 1
                    )
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
        }
    }

    override fun onExternalPageRequest(url: String) {
        startIntent(url)
    }

    companion object {
        private const val APKMIRROR_URL = "https://www.apkmirror.com/"
        private const val APKMIRROR_UPLOAD_URL = "https://www.apkmirror.com/apk-upload/"
        private val COLOR_STATES = arrayOf(
            intArrayOf(android.R.attr.state_checked),
            intArrayOf(-android.R.attr.state_checked)
        )
    }
}
