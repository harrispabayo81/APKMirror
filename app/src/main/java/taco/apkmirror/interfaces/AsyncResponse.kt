package taco.apkmirror.interfaces

interface AsyncResponse {
    fun onProcessFinish(themeColor: Int)
}
