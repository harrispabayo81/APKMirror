1.3.3:
- Re-enable shrinking release build
- Add Afrikaans translation (thanks to ninelima on GitLab!)
- Some internal improvements
